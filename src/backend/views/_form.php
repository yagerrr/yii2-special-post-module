<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yagerguo\yii2special\models\SpecialPost;
?>

<div class="special-post-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'content')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <?= $form->field($model, 'coverPicFile')->fileInput() ?>
    
    <?php if(!empty($model->coverPic)){ ?>
        <img src="<?= $model->coverPic ?>" class="form-uploaded-image" />
    <?php } ?>

    <?= $form->field($model, 'focusPicFile')->fileInput() ?>
    
    <?php if(!empty($model->focusPic)){ ?>
        <img src="<?= $model->focusPic ?>" class="form-uploaded-image" />
    <?php } ?>

    <?= $form->field($model, 'status')->dropDownList(['' => '选择状态'] + SpecialPost::statusData()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
