<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SpecialPost */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => '专题', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="special-post-view">

    <div class="well">
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'slug',
            'desc',
            'content:html',
            'coverPic:image',
            'focusPic:image',
            'status',
            'createdAt',
            'updatedAt',
            'deletedAt',
        ],
    ]) ?>

</div>
