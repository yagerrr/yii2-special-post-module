<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SpecialPost */

$this->title = '添加专题';
$this->params['breadcrumbs'][] = ['label' => '专题', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="special-post-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
