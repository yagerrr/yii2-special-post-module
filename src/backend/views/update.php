<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SpecialPost */

$this->title = $model->title;
$this->params['subTitle'] = '修改专题';
$this->params['breadcrumbs'][] = ['label' => '专题', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="special-post-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
