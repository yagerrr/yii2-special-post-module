<?php

namespace yagerguo\yii2special;

class SpecialModule extends \yii\base\Module
{
    public $controllerNamespace;
    
    public $frontendViewPath;
    
    public $uploadsFolder;
    
    public $imageUrl;

    public function init()
    {
        parent::init();
        
        if(empty($this->frontendViewPath)){
            $this->frontendViewPath = __DIR__ . '/frontend/views/';
        }
    }
}
