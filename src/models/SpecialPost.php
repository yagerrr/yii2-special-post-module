<?php

namespace yagerguo\yii2special\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "special_post".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $desc
 * @property string $content
 * @property string $coverPic
 * @property string $focusPic
 * @property integer $status
 * @property integer $createdAt
 * @property integer $updatedAt
 * @property integer $deletedAt
 */
class SpecialPost extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;
    
    public $coverPicFile;
    public $focusPicFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'special_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'desc', 'content'], 'required'],
            [['content', 'desc'], 'string'],
            [['coverPicFile', 'focusPicFile'], 'file'],
            [['status', 'createdAt', 'updatedAt', 'deletedAt'], 'integer'],
            [['title', 'slug', 'coverPic', 'focusPic'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'slug' => 'Slug',
            'desc' => '简述',
            'content' => '内容',
            'coverPic' => '封面图',
            'focusPic' => '焦点图',
            'status' => '状态',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'statusText' => '状态',
            'coverPicFile' => '封面图',
            'focusPicFile' => '焦点图',
        ];
    }

    /**
     * @inheritdoc
     * @return SpecialPostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SpecialPostQuery(get_called_class());
    }
    
    public static function statusData(){
        return [
            self::STATUS_ACTIVE => '开启',
            self::STATUS_DISABLED => '关闭'
        ];
    }
    
    public function getStatusText(){
        return self::statusData()[$this->status];
    }
    
    public function getRenderContent(){
        return Yii::$app->shortcodes->parse($this->content);
    }
    
    public function process($folder, $site){
       
        $this->save();
        
        $fileName  = 'coverPicFile';
        $this->$fileName = UploadedFile::getInstance($this, $fileName);
        if ($this->$fileName && $this->validate()) {
            $path = time() . rand(1000, 9999) . '.' . $this->$fileName->extension;
            $this->$fileName->saveAs( $folder . $path);
            $this->coverPic = $site . $path;
            $this->save();
        }
        
        $fileName  = 'focusPicFile';
        $this->$fileName = UploadedFile::getInstance($this, $fileName);
        if ($this->$fileName && $this->validate()) {
            $path = time() . rand(1000, 9999) . '.' . $this->$fileName->extension;
            $this->$fileName->saveAs( $folder . $path);
            $this->focusPic = $site . $path;
            $this->save();
        }
        
        return true;
    }
}
