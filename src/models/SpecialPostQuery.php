<?php

namespace yagerguo\yii2special\models;

/**
 * This is the ActiveQuery class for [[SpecialPost]].
 *
 * @see SpecialPost
 */
class SpecialPostQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['status' => SpecialPost::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return SpecialPost[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SpecialPost|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}