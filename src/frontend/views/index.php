<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '专题';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="special-post-index">
    
    <div class="well">
        <?= Html::a('添加专题', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    
    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [

                    'id',
                    'title',
                    'slug',
//                    'desc',
                     'coverPic:image',
//                     'focusPic',
                     'status',
                    // 'createdAt',
                    // 'updatedAt',
                    // 'deletedAt',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>
